def quicksort(arr):
    if len(arr) < 2:
        return arr
    else:
        pivot = arr[0]
        # subarray of all the elements less than the pivot
        less = [i for i in arr[1:] if i <= pivot]
        # subarray of all the elements greater than the pivot
        greater = [i for i in arr[1:] if i > pivot]
        return quicksort(less) + [pivot] + quicksort(greater)


quicksort([2, 3, 5, 6, 3, 78, 9, 1, 4, 55, 6])
