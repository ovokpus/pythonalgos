class BinarySearch():
    def search_iterative(self, my_list, item):
        low = 0
        high = len(my_list) - 1

        while low <= high:
            mid = (low + high) // 2
            guess = my_list[mid]

            if guess == item:
                return mid
            if guess > item:
                high = mid - 1
            else:
                low = mid + 1
        return None

    def search_recursive(self, mylist, low, high, item):
        if high >= low:
            mid = (high + low) // 2
            guess = mylist[mid]

            if guess == item:
                return mid
            elif guess > item:
                return self.search_recursive(mylist, low, mid - 1, item)
            else:
                return self.search_recursive(mylist, mid + 1, high, item)
        else:
            return None


bs = BinarySearch()

my_list = [1, 3, 5, 7, 9, 11, 13, 15]

iterative = bs.search_iterative(my_list, 15)
print(iterative)

recursive = bs.search_recursive(my_list, 0, 7, 15)
print(recursive)
