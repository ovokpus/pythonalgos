def lanternfish(days, state, memo):
    for value in range(max(9, max(state))):
        memo[value] = 0
    for element in state:
        memo[element] += 1

    for i in range(days):
        zeros = memo[0]
        memo[0] = 0
        for index in range(1, len(memo)):
            memo[index - 1] += memo[index]
            memo[index] = 0
        memo[6] += zeros
        memo[8] += zeros
            
    result = 0
    for key in memo:
        result += memo[key]
    
    return result

def lanternfish_after_days(days, initial_state):
    return lanternfish(days, initial_state, {})

print(lanternfish_after_days(18, [3, 4, 3, 1, 2]))
print(lanternfish_after_days(80, [3, 4, 3, 1, 2]))
print(lanternfish_after_days(400, [3, 4, 3, 1, 2]))