from stack import Stack

def reverse_string(stack, input_string):
    for i in range(len(input_string)):
        stack.push(input_string[i])
    rev_str = ""
    while not stack.is_empty():
        rev_str += stack.pop()

    return rev_str


stack = Stack()
input_str = "!evitacudE ot emocleW"
print(reverse_string(stack, input_str))
